<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:lx="http://linkeddata.overheid.nl/lx/"
  exclude-result-prefixes="xs lx"
>

  <!-- Extract metadata for Link eXtractor from HUDOC files.
    * There may be multiple documents with the same ECLI. Newer instances overwrite older ones.
    * HUDOC uses kpdate to select documents of the last week / month.
  -->

  <xsl:import href="hudoc-rules.xslt"/>

  <xsl:template match="results">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>

  <!--
    The result element contains metadata for a single document.
  -->
  <xsl:template match="result">
    <!-- Use only results that have an ECLI. -->
    <xsl:if test="value[data(.) ne '' and @name = 'ecli']">
      <xsl:copy>
        <!-- Values used for attributes. -->
        <xsl:apply-templates select="value[data(.) ne '' and @name = ('itemid', 'appno', 'decisiondate', 'judgementdate', 'kpdate', 'ecli')]"/>
        <!-- Values used for content. -->
        <xsl:apply-templates select="value[data(.) ne '' and @name = ('docname', 'respondent')]"/>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

  <xsl:template match="value[@name = ('itemid', 'ecli')]">
    <xsl:attribute name="{@name}" select="data(.)"/>
  </xsl:template>

  <xsl:template match="value[@name eq 'appno']">
    <xsl:attribute name="{@name}" select="replace(data(.), '[^0-9]', '')"/>
  </xsl:template>

  <xsl:template match="value[@name = ('decisiondate', 'judgementdate', 'kpdate')]">
    <xsl:attribute name="{@name}" select="lx:date(data(.))"/>
  </xsl:template>

  <!-- The docname is used to find the applicants. -->
  <!-- PROBLEM CASES:
    - CASE OF SULEJMANOVIC AND OTHERS AND SEJDOVIC AND SULEJMANOVIC v. ITALY
    - THE FIRESTONE TIRE AND RUBBER CO. FIRESTONE TIRE AND RUBBER CO. LTD AND THE INTERNATIONAL SYNTHETIC RUBBER CO. LTD v. THE UNITED KINGDOM
    - SARUPICI v. THE REPUBLIC OF MOLDOVA AND UKRAINE AND GANEA AND GHERSCOVICI v. THE REPUBLIC OF MOLDOVA
    -TUZ AND OTHERS v. TURKEY AND GUNGOR AND GOCMENOGLU v. TURKEY
    - MELLACHER AND ANOTHER, MÖLK AND OTHERS AND WEISS-TESSBACH AND ANOTHER v. AUSTRIA
    - KUDUMIJA v. BOSNIA AND HERZEGOVINA AND SERBIA AND REMENOVIĆ AND MAŠOVIĆ v. BOSNIA AND HERZEGOVINA
  -->
  <xsl:template match="value[@name eq 'docname']">
    <title>
      <xsl:value-of select="data(.)"/>
    </title>
    <xsl:if test="not(matches(data(.), $titleRE, 'i'))">
      <error>NO MATCH with expected title format: <xsl:value-of select="data(.)"/></error>
    </xsl:if>
    <xsl:variable name="applicants" select="replace(data(.), $titleRE, '$2', 'i')"/>
    <!-- Determine applicants. -->
    <xsl:for-each select="tokenize(normalize-space($applicants), $titleAndRE, 'i')">
      <xsl:variable name="applicant" select="lx:normalize-applicant(.)"/>
      <xsl:choose>
        <xsl:when test="matches($applicant, $titleOthersRE, 'i')">
          <cum_suis/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="code" select="lx:country-code($applicant)"/>
          <applicant>
            <xsl:attribute name="position" select="position()"/>
            <!-- If there is a code, the applicant is a country. -->
            <xsl:if test="$code">
              <xsl:attribute name="code" select="$code"/>
            </xsl:if>
            <xsl:value-of select="$applicant"/>
          </applicant>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <!-- Determine defendants. -->
    <!-- Often the title is followed by something like (No. 2) or (article 41), so this does not work. Use respondent elements instead.
    <xsl:variable name="defendants" select="replace(data(.), $titleRE, '$4', 'i')"/>
    <xsl:for-each select="tokenize($defendants, '\s+AND\s+')">
      <xsl:variable name="defendant" select="lower-case(replace(., '^THE\s+', ''))"/>
      <xsl:variable name="code" select="lx:country-code($defendant)"/>
      <xsl:if test="$code eq ''">
        <error>NO DEFENDANT CODE FOR <xsl:value-of select="$defendant"/></error>
      </xsl:if>
      <defendant code="{$code}">
        <xsl:value-of select="."/>
      </defendant>
    </xsl:for-each>
    -->
  </xsl:template>

  <xsl:template match="value[@name eq 'respondent']">
    <!-- Multiple respondents are separated by ';'. -->
    <xsl:for-each select="tokenize(data(.), '\s*;\s*')">
      <!-- Take the country-code of the country name of the respondent (code), to get a unique code for each country. -->
      <xsl:variable name="respondent" select="lx:country-name(.)"/>
      <respondent code="{lx:country-code($respondent)}">
        <xsl:value-of select="$respondent"/>
      </respondent>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
