<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xs"
>
  <!--
    Prepare the input to be written to a destination file, using the write-source transformer.
  -->

  <!-- Name of the destination file. -->
  <xsl:param name="src"/>

  <!-- If $append is non-empty, the fragment is appended at the end of the root element in the destination file.
       If $append is empty, a new destination file is created. -->
  <xsl:param name="append" select="''"/>

  <!-- If $select is non-empty (and $append is non-empty) elements with a name matching $select are appended to the destination file. -->
  <xsl:param name="select" select="''"/>

  <!-- If $keep-ancestors is non-empty and $select is non-empty, elements around the $select-ed element are kept.
       If $keep-ancestors is non-empty and $select is empty, the root-element of the input is copied around the source:write element. -->
  <xsl:param name="keep-ancestors" select="''"/>

  <!-- If $preserve-copy is non-empty, a copy of the input is kept in the output.
       If $preserve-copy is empty, the input is only written to the destination file. -->
  <xsl:param name="preserve-copy" select="''"/>

  <!-- If $mode = 'xml.zip' (and $append = '') a zip:archive is generated and you must call the write-source transformer like:
        <map:transform type="write-source">
          <map:parameter name="serializer" value="zip"/>
        </map:transform>
  -->
  <xsl:param name="mode" select="''"/>


  <xsl:template match="/*">
    <xsl:choose>
      <xsl:when test="$select != ''">
        <xsl:if test="$append = ''">
          <xsl:message terminate="yes">Write-source: You cannot 'select' elements if 'append' is false.</xsl:message>
        </xsl:if>
        <xsl:choose>
          <xsl:when test="$keep-ancestors != '' and local-name(.) != $select">
            <xsl:copy>
              <xsl:copy-of select="@*"/>
              <xsl:apply-templates select="node()"/>
              <xsl:if test="exists(//*[local-name(.) = $select])">
                <xsl:apply-templates select="." mode="write-source"/>
              </xsl:if>
            </xsl:copy>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="." mode="write-source"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise><!-- $select = '' -->
        <xsl:choose>
          <xsl:when test="$keep-ancestors != ''">
            <xsl:copy>
              <xsl:copy-of select="@*"/>
              <xsl:apply-templates select="." mode="write-source"/>
            </xsl:copy>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="." mode="write-source"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*[local-name(.) = $select]">
    <!-- The selected element is not copied, but written to the destination. -->
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="*" mode="write-source">
    <write-source>
      <xsl:if test="$preserve-copy != ''">
        <xsl:copy-of select="."/>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="$append != ''">
          <source:insert xmlns:source="http://apache.org/cocoon/source/1.0">
            <source:source>
              <xsl:value-of select="$src"/>
            </source:source>
            <source:path>/*</source:path>
            <source:fragment>
              <xsl:apply-templates select="." mode="fragment"/>
            </source:fragment>
          </source:insert>
        </xsl:when>
        <xsl:otherwise>
          <source:write xmlns:source="http://apache.org/cocoon/source/1.0">
            <source:source>
              <xsl:value-of select="$src"/>
            </source:source>
            <source:fragment>
              <xsl:apply-templates select="." mode="fragment"/>
            </source:fragment>
          </source:write>
        </xsl:otherwise>
      </xsl:choose>
    </write-source>
  </xsl:template>

  <!-- Get the indicated fragment to be written to the destination file. -->
  <xsl:template match="*" mode="fragment">
    <xsl:choose>
      <xsl:when test="$mode = 'xml.zip'">
        <zip:archive xmlns:zip="http://apache.org/cocoon/zip-archive/1.0">
          <zip:entry name="{replace($src, '^.*[/\\]|\.zip$', '')}" serializer="xml">
            <xsl:copy-of select="."/>
          </zip:entry>
        </zip:archive>
      </xsl:when>
      <xsl:when test="$select != ''">
        <xsl:copy-of select="//*[local-name(.) = $select]"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
