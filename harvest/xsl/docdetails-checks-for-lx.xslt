<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xs"
>

  <!-- Checks on the generated data for LX. -->

  <xsl:param name="file"/>

  <xsl:template match="result">
    <!--
      Determine the preferred date.
      Als check kan je vervolgens nog toepassen: de aldus gevonden datum moet – in omgekeerde volgorde - gelijk zijn aan het vierde element van ECLI + de eerste vier karakters van het vijfde element van ECLI.
    -->
    <xsl:variable name="date" as="xs:string?"
      select="if (@kpdate) then @kpdate else if (@judgementdate) then @judgementdate else @decisiondate"/>
    <xsl:variable name="eclidate"
      select="replace(@ecli, '^.+:.+:.+:(\d\d\d\d):(\d\d)(\d\d).*', '$1-$2-$3')"/>
    <!-- Errors generated in previous stylesheet(s) and new errors. -->
    <xsl:variable name="errors">
      <xsl:copy-of select=".//error"/>
      <xsl:if test="$date eq ''">
        <error>No date for <xsl:value-of select="@itemid"/>.</error>
      </xsl:if>
      <xsl:if test="$date ne $eclidate">
        <error>Date <xsl:value-of select="$date"/> differs from ECLI-date <xsl:value-of select="$eclidate"/>.</error>
      </xsl:if>
    </xsl:variable>
    <!--Copy the result, add the date. -->
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="date" select="$date"/>
      <xsl:if test="count($errors/error) gt 0">
        <xsl:message><xsl:value-of select="$file"/>: Found <xsl:value-of select="count($errors/error)"/> error(s) in <xsl:value-of select="@itemid"/>: <xsl:value-of select="string-join(($errors/error), '&#x0A;')"/></xsl:message>
        <xsl:copy-of select="$errors/error"/>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="error">
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
