<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:req="http://apache.org/cocoon/request/2.0"
  exclude-result-prefixes="xs req"
>
  <!--
    Make a query to send to HUDOC for retrieving all documents after a certain date, one chunk at a time. -->
  -->

  <xsl:param name="default-start"/>
  <xsl:param name="default-length"/>

  <!-- Make the root element available in xsl:function. -->
  <xsl:variable name="root" select="/"/>

  <!-- Access request parameters. -->
  <xsl:function name="req:param" as="xs:string">
    <xsl:param name="name" as="xs:string"/>
    <xsl:param name="default"/>
    <xsl:variable name="value" select="$root//req:requestParameters/req:parameter[@name eq $name]/req:value"/>
    <xsl:value-of select="if (exists($value)) then $value else $default"/>
  </xsl:function>

  <xsl:variable name="today" select="format-date(current-date(), '[Y0001]-[M01]-[D01]')"/>

  <!-- Transform the request parameters into a query.
    This is a sparql:query, because the sparql-transformer provides a flexible way to make HTTP-requests.
    It really is a Sharepoint query.
  -->
  <xsl:template match="/">
    <!-- The original select attribute is:
      sparql:select="sharepointid,Rank,itemid,docname,doctype,application,appno,conclusion,importance,originatingbody,typedescription,kpdate,kpdateAsText,documentcollectionid,documentcollectionid2,languageisocode,extractedappno,isplaceholder,doctypebranch,respondent,respondentOrderEng,ecli"
      It turns out that you can retrieve all the required metadata for all documents in the list by tweaking the select attribute.
    -->
    <sparql:query
      xmlns:sparql="http://apache.org/cocoon/sparql/1.0"
      xmlns:http="http://www.w3.org/2006/http#"
      src="http://hudoc.echr.coe.int/app/query/results"
      method="GET"
      content="text"
      parse="text"
      sparql:start="{req:param('start', xs:integer($default-start))}"
      sparql:length="{req:param('length', xs:integer($default-length))}"
      sparql:sort=""
      sparql:rankingModelId="4180000c-8692-45ca-ad63-74bc4163871b"
      sparql:select="itemid,applicability,appno,article,conclusion,decisiondate,docname,documentcollectionid,documentcollectionid2,doctype,externalsources,importance,introductiondate,issue,judgementdate,kpthesaurus,meetingnumber,originatingbody,publishedby,referencedate,kpdate,reportdate,representedby,resolutiondate,resolutionnumber,respondent,rulesofcourt,separateopinion,scl,typedescription,ecli"
      parameter-name="query"
    >
      <xsl:text>(((((((((((((((((((( contentsitename:ECHR AND (NOT (doctype=PR OR doctype=HFCOMOLD OR doctype=HECOMOLD)) AND ((languageisocode="ENG")) AND ((documentcollectionid="JUDGMENTS") OR (documentcollectionid="DECGRANDCHAMBER") OR (documentcollectionid="ADMISSIBILITY") OR (documentcollectionid="ADMISSIBILITYCOM") OR (documentcollectionid="DECCOMMISSION") OR (documentcollectionid="REPORTS")) AND (kpdate>="</xsl:text><xsl:value-of select="req:param('date', $today)"/><xsl:text>T00:00:00.0Z") ) XRANK(cb=14) doctypebranch:GRANDCHAMBER) XRANK(cb=13) doctypebranch:DECGRANDCHAMBER) XRANK(cb=12) doctypebranch:CHAMBER) XRANK(cb=11) doctypebranch:ADMISSIBILITY) XRANK(cb=10) doctypebranch:COMMITTEE) XRANK(cb=9) doctypebranch:ADMISSIBILITYCOM) XRANK(cb=8) doctypebranch:DECCOMMISSION) XRANK(cb=7) doctypebranch:COMMUNICATEDCASES) XRANK(cb=6) doctypebranch:CLIN) XRANK(cb=5) doctypebranch:ADVISORYOPINIONS) XRANK(cb=4) doctypebranch:REPORTS) XRANK(cb=3) doctypebranch:EXECUTION) XRANK(cb=2) doctypebranch:MERITS) XRANK(cb=1) doctypebranch:SCREENINGPANEL) XRANK(cb=4) importance:1) XRANK(cb=3) importance:2) XRANK(cb=2) importance:3) XRANK(cb=1) importance:4) XRANK(cb=2) languageisocode:ENG) XRANK(cb=1) languageisocode:FRE</xsl:text>
<!-- last week query:
(((((((((((((((((((( contentsitename:ECHR AND (NOT (doctype=PR OR doctype=HFCOMOLD OR doctype=HECOMOLD
)) AND ((documentcollectionid="GRANDCHAMBER") OR (documentcollectionid="CHAMBER")) AND (kpdate>="2016-01-27T00
:00:00.0Z" AND kpdate<="2016-02-03T00:00:00.0Z")) XRANK(cb=14) doctypebranch:GRANDCHAMBER) XRANK(cb=13
) doctypebranch:DECGRANDCHAMBER) XRANK(cb=12) doctypebranch:CHAMBER) XRANK(cb=11) doctypebranch:ADMISSIBILITY
) XRANK(cb=10) doctypebranch:COMMITTEE) XRANK(cb=9) doctypebranch:ADMISSIBILITYCOM) XRANK(cb=8) doctypebranch
:DECCOMMISSION) XRANK(cb=7) doctypebranch:COMMUNICATEDCASES) XRANK(cb=6) doctypebranch:CLIN) XRANK(cb
=5) doctypebranch:ADVISORYOPINIONS) XRANK(cb=4) doctypebranch:REPORTS) XRANK(cb=3) doctypebranch:EXECUTION
) XRANK(cb=2) doctypebranch:MERITS) XRANK(cb=1) doctypebranch:SCREENINGPANEL) XRANK(cb=4) importance
:1) XRANK(cb=3) importance:2) XRANK(cb=2) importance:3) XRANK(cb=1) importance:4) XRANK(cb=2) languageisocode
:ENG) XRANK(cb=1) languageisocode:FRE
-->
    </sparql:query>
  </xsl:template>

</xsl:stylesheet>
