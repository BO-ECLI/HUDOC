<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:sparql="http://apache.org/cocoon/sparql/1.0"
  xmlns:json="urn:xslt-json-parser:json-parser"
  exclude-result-prefixes="xs sparql json"
>
  <!--
    Adapt the JSON parser to deal with the output of the sparql-transformer, and some HUDOC oddities.
    This gives a neat <results> list.
  -->

  <xsl:import href="json-parser.xslt"/>

  <xsl:template match="/*">
    <xsl:choose>
      <xsl:when test="self::sparql:error">
        <xsl:copy-of select="."/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="resultset" select="json:parse-json(.)"/>
        <xsl:variable name="results" select="$resultset/pair[@name='results']/array/object"/>
        <results total-count="{data($resultset/pair[@name='resultcount'])}" actual-count="{count($results)}">
          <xsl:apply-templates select="$results"/>
        </results>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="object">
    <result>
      <xsl:apply-templates select="pair[@name='columns']/object/pair"/>
    </result>
  </xsl:template>

  <xsl:template match="pair">
    <xsl:if test="count(*) gt 1">
      <xsl:message terminate="yes">Unexpected multivalued pair: <xsl:copy-of select="."/></xsl:message>
    </xsl:if>
    <value>
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="type" select="name(*)"/>
      <!-- HUDOC appears to escape '/'. -->
      <xsl:value-of select="replace(data(*), '\\/', '/')"/>
    </value>
  </xsl:template>

</xsl:stylesheet>
