<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="xs"
>

  <xsl:template match="value[@name = 'docname']">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:choose>
        <xsl:when test="data(.) eq 'IMMOBILIARE LIBECCIO'">
          <xsl:text>IMMOBILIARE LIBECCIO V. ITALY</xsl:text>
        </xsl:when>
        <xsl:when test="data(.) eq 'KOVAR, Alexander'">
          <xsl:text>KOVAR, Alexander V. AUSTRIA</xsl:text>
        </xsl:when>
        <xsl:when test="data(.) eq 'DUGGAN, Neal'">
          <xsl:text>DUGGAN, Neal V. Ireland</xsl:text>
        </xsl:when>
        <xsl:when test="data(.) eq 'DECISION BY THE COMMISSION ON THE ADMISSIBILITY'">
          <xsl:text>DECISION BY THE COMMISSION ON THE ADMISSIBILITY V. GERMANY</xsl:text>
        </xsl:when>
        <xsl:when test="data(.) eq 'SARUPICI v. THE REPUBLIC OF MOLDOVA AND UKRAINE AND GANEA AND GHERSCOVICI v. THE REPUBLIC OF MOLDOVA'">
          <xsl:text>SARUPICI AND GANEA AND GHERSCOVICI v. THE REPUBLIC OF MOLDOVA AND UKRAINE</xsl:text><!-- otherwise we cannot parse -->
        </xsl:when>
        <xsl:when test="data(.) eq 'KUDUMIJA v. BOSNIA AND HERZEGOVINA AND SERBIA AND REMENOVIĆ AND MAŠOVIĆ v. BOSNIA AND HERZEGOVINA'">
          <xsl:text>KUDUMIJA AND REMENOVIĆ AND MAŠOVIĆ v. BOSNIA AND HERZEGOVINA AND SERBIA</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="node()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="result[data(value[@name eq 'itemid']) eq '001-45495']/value[@name = 'respondent' and data(.) eq 'CHL']">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:text>SWE</xsl:text>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
