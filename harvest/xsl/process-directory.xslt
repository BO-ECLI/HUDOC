<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:dir="http://apache.org/cocoon/directory/2.0"
  exclude-result-prefixes="xs dir"
>

  <xsl:param name="root"/>
  <xsl:param name="url"/>

  <xsl:template match="/">
    <xsl:message>Start processing HUDOC results in <xsl:value-of select="$root"/>, using url <xsl:value-of select="$url"/>.</xsl:message>
<!--    <info><root><xsl:value-of select="$root"/></root><url><xsl:value-of select="$url"/></url></info>-->
    <xsl:variable name="results">
      <xsl:apply-templates select="//dir:file"/>
    </xsl:variable>
    <xsl:message>Ready processing HUDOC results.</xsl:message>
    <xsl:element name="{name($results/*[1])}">
      <xsl:copy-of select="$results/*/node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="dir:file">
    <xsl:variable name="path" select="string-join(($root, ancestor-or-self::dir:*/@name), '/')"/>
    <xsl:message>Processing <xsl:value-of select="$path"/></xsl:message>
    <xsl:copy-of select="document(concat($url, 'file:', $path))"/>
  </xsl:template>

</xsl:stylesheet>
