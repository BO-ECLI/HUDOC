<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:lx="http://linkeddata.overheid.nl/lx/"
  exclude-result-prefixes="xs lx"
>

<!-- This file contains rules for parsing and correcting HUDOC elements.
  It is common to all processing stylesheets for HUDOC.
-->


  <!-- Translations according to HUDOC. -->

  <!-- Read translations and prepare for country code translations.
    The order of the translations is very important!
    In case of ambiguity, the first alternative is taken.]
    The order in the translations.xml file has been changed so the preferred country code comes first.
  -->
  <xsl:variable name="translations" select="document('translations.xml')"/>
  <xsl:variable name="translations-country" select="$translations//results[@title='Country Codes']/result"/>

  <!-- If there are multiple country codes / names for a country, take the first. -->
  <xsl:function name="lx:country-code" as="xs:string">
    <xsl:param name="name" as="xs:string"/>
    <xsl:variable name="country" select="$translations-country[lower-case($name) = (for $tn in TermName return lower-case($tn))]"/>
    <xsl:value-of select="$country[1]/TermValue"/>
  </xsl:function>

  <!-- If there are multiple country codes / names for a country, take the first. -->
  <xsl:function name="lx:country-name" as="xs:string">
    <xsl:param name="code" as="xs:string"/>
    <xsl:value-of select="$translations-country[TermValue=$code][1]/TermName[1]"/>
  </xsl:function>


  <!-- Other conversions. -->

  <!-- Date conversion. -->
  <xsl:function name="lx:date" as="xs:string">
    <xsl:param name="date" as="xs:string*"/>
    <xsl:if test="count($date) gt 1">
      <xsl:message terminate="yes">MULTIPLE DATES: <xsl:value-of select="string-join($date, ', ')"/></xsl:message>
    </xsl:if>
    <!-- Sometimes we have [3/8/2016 12:00:00 AM] because HUDOC randomly decides to use English date notation. -->
    <xsl:variable name="dateRE" select="'^(\d\d)/(\d\d)/(\d\d\d\d) 00:00:00$'"/>
    <xsl:variable name="GBdateRE" select="'^(\d\d?)/(\d\d?)/(\d\d\d\d) 12:00:00 AM$'"/>
    <xsl:choose>
      <xsl:when test="matches($date, $dateRE)">
        <xsl:value-of select="replace($date, $dateRE, '$3-$2-$1')"/>
      </xsl:when>
      <xsl:when test="matches($date, $GBdateRE)">
        <xsl:analyze-string select="$date" regex="{$GBdateRE}">
          <xsl:matching-substring>
            <xsl:value-of select="
              string-join((regex-group(3),
                           format-number(xs:integer(regex-group(1)), '00'),
                           format-number(xs:integer(regex-group(2)), '00')),
                          '-')"/>
          </xsl:matching-substring>
        </xsl:analyze-string>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">
          <xsl:value-of select="concat('BAD DATE: [', $date, ']')"/>
        </xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>


  <!-- Processing document titles. -->

  <!-- The title must match this pattern, to find components. To be used case-insensitively.
    - "CASE OF" is optional.
    - The applicants string is the longest possible, because 'v.' may be part of an applicant.
      However, this fails on "CASE OF GUERRERA AND FUSCO v. ITALY (Rev.)". Therefore...
    - The defendant must contain at least one letter.
  -->
  <xsl:variable name="titleRE" select="
    '^(CASE OF\s+)?(.*\S)?(\s*[VC]\.\s*|\s+against\s+|\s+contre\s+)(.*[A-Za-z].*)$'
  "/>

  <!-- Pattern for 'and' separating applicants. To be used case-insensitively.
    BEWARE: When we split off too small applicants, "REALINVEST, S.R.O." is seen as two applicants, one of which is anonymized.
  -->
  <xsl:variable name="titleAndRE" select="'\s+AND(\s*;)?\s+|\s*[;,](\s*AND)?\s+'"/>

  <!-- Pattern for 'others' in applicants. -->
  <xsl:variable name="titleOthersRE" select="'^OTHERS\.?$'"/>

  <!-- Normalize applicant:
    * Anonymized applicants sometimes have one initial without '.'. We add the '.' here.
    * For some reason, some applicant names end with ',' (e.g., ECLI:CE:ECHR:2011:0224JUD003390804).
  -->
  <xsl:function name="lx:normalize-applicant" as="xs:string">
    <xsl:param name="applicant" as="xs:string"/>
    <xsl:value-of select="
      replace(
        replace(
          normalize-space($applicant),
          '^(.)$', '$1.'),
        ',$', ''
      )"/>
  </xsl:function>

  <!-- Determine if the applicant is in 'long' format, i.e., not anonymized initials or a state.
    BEWARE: The same rule should be followed by the grammar hudoc.waxeye.
  -->
  <xsl:function name="lx:long-applicant" as="xs:boolean">
    <xsl:param name="applicant" as="node()"/>
    <xsl:value-of select="
      not($applicant/@code or
          matches(data($applicant), '^\s*.\.?\s*(.\.?\s*)?(.\.\s*)*$')
         )
    "/>
  </xsl:function>

</xsl:stylesheet>
