<?xml version="1.0" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:req="http://apache.org/cocoon/request/2.0"
  exclude-result-prefixes="xs req"
>
  <!--
    Make a query for document details.
    This is not used at this moment, but kept for later use.
  -->

  <!-- Make the root element available in xsl:function. -->
  <xsl:variable name="root" select="/"/>

  <!-- Access request parameters. -->
  <xsl:function name="req:param" as="xs:string">
    <xsl:param name="name" as="xs:string"/>
    <xsl:param name="default"/>
    <xsl:variable name="value" select="$root//req:requestParameters/req:parameter[@name eq $name]/req:value"/>
    <xsl:value-of select="if (exists($value)) then $value else $default"/>
  </xsl:function>

  <xsl:template match="/">
    <sparql:query
      xmlns:sparql="http://apache.org/cocoon/sparql/1.0"
      xmlns:http="http://www.w3.org/2006/http#"
      src="http://hudoc.echr.coe.int/app/query/results"
      method="GET"
      content="text"
      parse="text"
      sparql:start="0"
      sparql:length="1"
      sparql:sort=""
      sparql:select="itemid,applicability,appno,article,conclusion,decisiondate,docname,documentcollectionid,documentcollectionid2,doctype,externalsources,importance,introductiondate,issue,judgementdate,kpthesaurus,meetingnumber,originatingbody,publishedby,referencedate,kpdate,reportdate,representedby,resolutiondate,resolutionnumber,respondent,rulesofcourt,separateopinion,scl,typedescription,ecli"
      parameter-name="query"
    >
      <xsl:text>(contentsitename=ECHR) AND </xsl:text>
      <xsl:value-of select="req:param('itemid', '')"/>
    </sparql:query>
  </xsl:template>

</xsl:stylesheet>
