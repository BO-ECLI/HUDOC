#!/usr/bin/env bash

docker run -i -p 8123:8123 -v `pwd`/data:/hudoc/data  -v `pwd`/results:/hudoc/results -v `pwd`/work:/hudoc/work -v `pwd`/logs:/hudoc/run/cocoon-2.1.12/build/webapp/WEB-INF/logs -w /hudoc/run/cocoon-2.1.12 hudoc ./cocoon.sh

# To make changes to the code while the Docker container is running: -v `pwd`/harvest:/hudoc/harvest
