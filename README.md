# HUDOC harvester for BO-ECLI

The code in this repository was taken from the KOOP Link eXtractor (LX).
It harvests metadata from [HUDOC](http://hudoc.echr.coe.int/eng).
The code has been separated from the Link eXtractor to make it available to the BO-ECLI project.


# Technical basis.

Like the Link eXtractor, the HUDOC harvester is an XML application based on the [Apache Cocoon 2.1](http://cocoon.apache.org/2.1/) framework.
It uses a mix of XSLT and custom-made Java components, which are assembled into processing pipelines.

Although Cocoon 2.1 is quite old and not very actively developed, it remains the most efficient and flexible XML processing framework.
It has adapters for a wide range of data formats.
For example, it is possible to query an RDF database using SPARQL, transform the data using XSLT and write (serialize) it to an Excel workbook.


# Installation instructions

The installation proceeds in several steps.

1. Check out this repo.
   Check it out on the machine you wish to run the HUDOC software on. Any machine with sufficient memory (4GB) will do.
   
1. Install Docker.
   Install Docker on the machine you wish to run the HUDOC software on.
   
1. Execute the script ./hudoc-build.sh
   This will build the Docker image used to run the HUDOC software.

# Run the HUDOC harvester robot

1. Execute the./hudoc-run.sh script.
   This will run the Docker image created in the previous step. When you see a line "Started SocketListener on 0.0.0.0:8123" in the console then the HUDOC software is running.

1. Start the HUDOC-robot-harvester.
   Point a webbrowser to http://localhost:8123/harvest/
   When you are running the Docker on a different machine, make sure you replace localhost with the IP of the machine, make sure any firewalls etc. allow
   traffic to port 8123 on that machine, etc.
   This will run for some time, there's a progress bar at the bottom of the window. Keep the window open to keep the robot happy and harvesting.
   
1. Collect the results.
   When the harvester robot is done, point your browser to http://localhost:8123/harvest/collect
   This will process and collect all the results from the previous step in a zip in the data folder on the machine the Docker image runs on.

# Inspecting the Docker image

1. Use the ./hudoc-inspect.sh script.
   Use the ./hudoc-inspect.sh script to run a container and inspect the software and data.
   
# Further developing the HUDOC-software

1. Map the /hudoc/harvest folder
   Map the /hudoc/harvest folder in the Docker image to the hudoc folder on your dev-machine to facilitate easy development of the HUDOC software, see ./hudoc-run.sh for details.