package eu.boecli.hudocapi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class MemDb {

	final ArrayList<HashMap<String, String>> records;
	final TreeMap<String, Integer> fields;
	final HashMap<String, List<String>> uniqueValues;

	MemDb() throws DatatypeConfigurationException {
		datatypeFactory = DatatypeFactory.newInstance();
		records = new ArrayList<HashMap<String, String>>();
		TreeSet<String> fields = new TreeSet<String>();
		HashMap<String, TreeSet<String>> unique = new HashMap<String, TreeSet<String>>();
		File f = new File("../results/");
		XMLInputFactory factory = XMLInputFactory.newInstance();
		for (File file : f.listFiles()) {
			if (file.isFile() && file.getName().endsWith(".xml")) {
				addRecords(factory, fields, unique, file);
				System.out.println(file + "\t" + records.size());
			}
		}
		this.fields = setToMap(fields, unique);
		this.uniqueValues = new HashMap<String, List<String>>();
		for (String name : unique.keySet()) {
			this.uniqueValues.put(name, setToList(unique.get(name)));
		}
	}

	private ArrayList<String> setToList(Set<String> set) {
		ArrayList<String> list = new ArrayList<String>(set.size());
		for (String v : set) {
			list.add(v);
		}
		return list;
	}

	private TreeMap<String, Integer> setToMap(Set<String> set,
			HashMap<String, TreeSet<String>> unique) {
		TreeMap<String, Integer> map = new TreeMap<String, Integer>();
		for (String v : set) {
			map.put(v, unique.get(v).size());
		}
		return map;
	}

	private void addRecords(XMLInputFactory factory, TreeSet<String> fields,
			HashMap<String, TreeSet<String>> unique, File xml) {
		try {
			Reader r = new BufferedReader(new FileReader(xml));
			XMLStreamReader streamReader = factory.createXMLStreamReader(r);
			addRecords(streamReader, fields, unique);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private static void add(HashMap<String, TreeSet<String>> unique,
			String name, String value) {
		TreeSet<String> set = unique.get(name);
		if (set == null) {
			set = new TreeSet<String>();
			unique.put(name, set);
		}
		set.add(value);
	}

	private void addRecords(XMLStreamReader in, TreeSet<String> fields,
			HashMap<String, TreeSet<String>> unique)
			throws XMLStreamException, ParseException {
		int level = 0;
		String name = null;
		String value = null;
		HashMap<String, String> record = null;
		while (in.hasNext()) {
			in.next();
			int type = in.getEventType();
			if (type == XMLStreamReader.START_ELEMENT) {
				if (level == 0
						&& in.getName().getLocalPart().equals("results")) {
					level += 1;
				} else if (level == 1
						&& in.getName().getLocalPart().equals("result")) {
					level += 1;
					record = new HashMap<String, String>();
				} else if (level == 2
						&& in.getName().getLocalPart().equals("value")) {
					level += 1;
					name = in.getAttributeValue(null, "name");
					fields.add(name);
					value = "";
				} else {
					System.err.println("Unsupported element!");
					System.err.println(in.getName());
				}
			} else if (type == XMLStreamReader.END_ELEMENT) {
				if (level == 3 && record != null) {
					String parsedValue = parseValue(name, value);
					record.put(name, parsedValue);
					add(unique, name, parsedValue);
					name = null;
					value = null;
				} else if (level == 2 && record != null) {
					records.add(record);
					record = null;
				} else if (level != 1) {
					System.err.println("Unexpected element end. " + level);
				}
				level -= 1;
			} else if (level == 3 && type == XMLStreamReader.CHARACTERS
					&& value != null) {
				value += in.getText();
			} else if (type != XMLStreamReader.START_DOCUMENT
					&& type != XMLStreamReader.END_DOCUMENT) {
				System.err.println(
						"unknown event " + in.getEventType() + " " + level);
			}
		}
	}

	private final DatatypeFactory datatypeFactory;

	private final DateFormat kpdate = new SimpleDateFormat("M/d/Y kk:mm:ss a");

	private final DateFormat decisiondate = new SimpleDateFormat(
			"M/d/Y 00:00:00");

	private XMLGregorianCalendar toXml(Date date) {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		return datatypeFactory.newXMLGregorianCalendar(c);
	}

	// simplify the dates to be YYYY-MM-DD
	private String parseValue(String name, String value) throws ParseException {
		if ("".equals(value)) {
			return value;
		} else if ("kpdate".equals(name)) {
			Date date = kpdate.parse(value);
			return toXml(date).toString().substring(0, 10);
		} else if ("decisiondate".equals(name)
				|| "introductiondate".equals(name)
				|| "referencedate".equals(name) || "reportdate".equals(name)
				|| "resolutiondate".equals(name)) {
			return toXml(decisiondate.parse(value)).toString().substring(0, 10);
		}
		return value;
	}

	public TreeMap<String, Integer> getFields() {
		return fields;
	}

	public List<String> getUniqueValues(String field) {
		return this.uniqueValues.get(field);
	}

	public boolean hasField(String field) {
		return this.uniqueValues.containsKey(field);
	}

	// Return all records that have the same values for the fields the ones
	// in the input map.
	public List<HashMap<String, String>> findReferences(
			HashMap<String, String> fields) {
		ArrayList<HashMap<String, String>> res = new ArrayList<HashMap<String, String>>();
		Set<Entry<String, String>> entrySet = fields.entrySet();
		for (HashMap<String, String> r : this.records) {
			boolean match = true;
			Iterator<Entry<String, String>> i = entrySet.iterator();
			while (i.hasNext() && match) {
				Entry<String, String> e = i.next();
				match &= e.getValue().equals(r.get(e.getKey()));
			}
			if (match) {
				res.add(r);
			}
		}
		return res;
	}
}
