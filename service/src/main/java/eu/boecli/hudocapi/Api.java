package eu.boecli.hudocapi;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import eu.boecli.hudocapi.schema.Field;
import eu.boecli.hudocapi.schema.GetFieldsOutput;
import eu.boecli.hudocapi.schema.GetUniqueValuesOutput;
import eu.boecli.hudocapi.schema.MatchRecordsInput;
import eu.boecli.hudocapi.schema.MatchRecordsOutput;
import eu.boecli.hudocapi.schema.MatchRecordsOutput.Record;

public class Api {

	/**
	 * Get the names of all the fields provided in the reference database.
	 */
	GetFieldsOutput getFields() {
		GetFieldsOutput out = new GetFieldsOutput();
		for (Entry<String, Integer> field : App.db.getFields().entrySet()) {
			GetFieldsOutput.Field f = new GetFieldsOutput.Field();
			f.setValue(field.getKey());
			f.setCount(field.getValue().longValue());
			out.getField().add(f);
		}
		return out;
	}

	/**
	 * Get all the unique values for a particular field.
	 */
	GetUniqueValuesOutput getUniqueValues(String field) {
		GetUniqueValuesOutput out = new GetUniqueValuesOutput();
		List<String> vals = App.db.getUniqueValues(field);
		if (vals != null) {
			for (String val : vals) {
				out.getValue().add(val);
			}
		} else {
			System.err.println("field not found! '" + field + "'");
		}
		return out;
	}

	/**
	 * Get all records where the field values match. The match is an AND match
	 * on all the fields and values. Fields that are not in the database are
	 * ignored.
	 */
	MatchRecordsOutput matchRecords(MatchRecordsInput fieldValues) {
		HashMap<String, String> fields = new HashMap<String, String>();
		for (eu.boecli.hudocapi.schema.Field field : fieldValues.getField()) {
			// check if any field is not present
			if (App.db.hasField(field.getName())) {
				fields.put(field.getName(), field.getValue());
			}
		}
		List<HashMap<String, String>> rs = App.db.findReferences(fields);
		MatchRecordsOutput out = new MatchRecordsOutput();
		for (HashMap<String, String> r : rs) {
			Record or = new Record();
			for (Entry<String, String> f : r.entrySet()) {
				Field field = new Field();
				field.setName(f.getKey());
				field.setValue(f.getValue());
				or.getField().add(field);
			}
			out.getRecord().add(or);
		}
		return out;
	}

}
