package eu.boecli.hudocapi;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.xml.sax.SAXException;

import eu.boecli.hudocapi.Validator.Direction;
import eu.boecli.hudocapi.schema.GetFieldsOutput;
import eu.boecli.hudocapi.schema.GetUniqueValuesOutput;
import eu.boecli.hudocapi.schema.MatchRecordsInput;
import eu.boecli.hudocapi.schema.MatchRecordsOutput;

/**
 * EntryPoint exposes the java API Api as a REST API. All input and output is
 * validated against the XML Schema 'hudocapi.xsd'. All functionality is
 * available as XML and JSON.
 */
@Path("hudoc")
public class EntryPoint {

	final private Validator validator;
	final private Api api;

	public EntryPoint() throws SAXException {
		validator = new Validator();
		api = new Api();
	}

	@GET
	@Path("fields")
	@Produces(MediaType.TEXT_XML)
	public GetFieldsOutput get_fields_xml() {
		GetFieldsOutput output = api.getFields();
		return validator.validate(Direction.OUTGOING, output,
				GetFieldsOutput.class);
	}

	@GET
	@Path("fields")
	@Produces(MediaType.APPLICATION_JSON)
	public GetFieldsOutput get_fields_json() {
		return this.get_fields_xml();
	}

	@GET
	@Path("unique-values/{field}")
	@Produces(MediaType.TEXT_XML)
	public GetUniqueValuesOutput get_unique_fields_xml(
			@PathParam("field") String field) {
		GetUniqueValuesOutput output = api.getUniqueValues(field);
		return validator.validate(Direction.OUTGOING, output,
				GetUniqueValuesOutput.class);
	}

	@GET
	@Path("unique-values/{field}")
	@Produces(MediaType.APPLICATION_JSON)
	public GetUniqueValuesOutput get_unique_fields_json(
			@PathParam("field") String field) {
		return this.get_unique_fields_xml(field);
	}

	@POST
	@Path("match-records")
	@Consumes(MediaType.TEXT_XML)
	@Produces(MediaType.TEXT_XML)
	public MatchRecordsOutput match_records_xml(
			@Valid MatchRecordsInput input) {
		validator.validate(Direction.INCOMING, input, MatchRecordsInput.class);
		MatchRecordsOutput output = api.matchRecords(input);
		return validator.validate(Direction.OUTGOING, output,
				MatchRecordsOutput.class);
	}

	@POST
	@Path("match-records")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MatchRecordsOutput match_records_json(
			@Valid MatchRecordsInput input) {
		return this.match_records_xml(input);
	}
}
