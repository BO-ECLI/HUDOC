package eu.boecli.hudocapi;

import javax.xml.datatype.DatatypeConfigurationException;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class App {

	public static final MemDb db;

	static {
		MemDb db_;
		try {
			db_ = new MemDb();
		} catch (DatatypeConfigurationException e) {
			db_ = null;
			e.printStackTrace();
		}
		db = db_;
	}

	public static void main(String[] args) throws Exception {
		ServletContextHandler context = new ServletContextHandler(
				ServletContextHandler.SESSIONS);
		context.setContextPath("/");

		Server jettyServer = new Server(8090);
		jettyServer.setHandler(context);
		context.addServlet(TestEntryPoint.class, "/test/*");
		add(context, "/*", EntryPoint.class);

		try {
			jettyServer.start();
			jettyServer.join();
		} finally {
			jettyServer.destroy();
		}
	}

	private static void add(ServletContextHandler context, String pathSpec,
			Class<?> c) {
		ServletHolder jerseyServlet = context.addServlet(ServletContainer.class,
				pathSpec);
		jerseyServlet.setInitOrder(0);
		// Tells the Jersey Servlet which REST service/class to load.
		jerseyServlet.setInitParameter(
				"jersey.config.server.provider.classnames",
				c.getCanonicalName());
	}
}
