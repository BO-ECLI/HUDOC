"use strict";

const fields = [];
const uniqueValues = {};

function doJson(path, data, callback) {
    var xhr = new XMLHttpRequest(),
        method = "GET";
    if (data !== null) {
        method = "POST";
        data = JSON.stringify(data, null, "  ");
    }
    function write(field, m) {
        empty(field);
        var msg;
        msg = method + " " + path;
        if (m) {
            msg += "\n" + m;
        }
        field.appendChild(document.createTextNode(msg));
    }
    xhr.open(method, path, true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    function handleResult() {
        if (xhr.readyState === 4) {
            if (xhr.status < 200 || xhr.status >= 300) {
                if (callback) {
                    callback(xhr.statusText);
                }
                return;
            }
            var j;
            try {
                j = JSON.parse(xhr.responseText);
            } catch (e) {
                error("Invalid JSON. " + e);
                return;
            }
            if (j["annotated-text"]) {
                setAnnotatedText(j["annotated-text"]);
            }
            if (callback) {
                callback(null, j);
            }
        }
    }
    xhr.onreadystatechange = handleResult;
    xhr.send(data);
}

function setRecords(json) {
    const records = document.getElementById('records');
    clear(records);
    json.record.forEach(record => {
        const table = document.createElement("table");
        for (const i in record.field) {
            const field = record.field[i];
            const tr = document.createElement("tr");
            const td1 = document.createElement("td");
            const td2 = document.createElement("td");
            td1.appendChild(document.createTextNode(field.name));
            td2.appendChild(document.createTextNode(field.value));
            tr.appendChild(td1);
            tr.appendChild(td2);
            table.appendChild(tr);
        }
        records.appendChild(table);
        records.appendChild(document.createElement("hr"));
    });
}

function matchReferences() {
    const field = [];
    let line = document.getElementById('filters').firstElementChild;
    while (line) {
        const name = line.firstElementChild.value;
        const value = line.firstElementChild.nextElementSibling.value;
        field.push({name, value});
console.log(field + " " + value);
        line = line.nextElementSibling;
    }
    doJson("/hudoc/match-records", {field}, (err, json) => {
        setRecords(json);
    });
}

function clear(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function setOptions(selector, options) {
    const parent = selector.parentNode;
    const next = selector.nextSibling;
    if (parent) {
        parent.removeChild(selector);
    }
    clear(selector);
    options.forEach((field) => {
        const option = document.createElement("option");
        const value = field.value || field;
        const text = field.value ? field.value + " (" + field.count + ")" : field;
        option.setAttribute("value", value);
        option.appendChild(document.createTextNode(text));
        selector.appendChild(option);
    });
    if (parent) {
        parent.insertBefore(selector, next);
    }
}

function getUniqueValues(field, callback) {
    if (uniqueValues[field]) {
        return callback(uniqueValues[field]);
    }
    doJson("/hudoc/unique-values/" + field, null, (err, json) => {
        if (err) {
            alert(err);
        }
        if (json) {
            uniqueValues[field] = json.value;
            callback(json.value);
        }
    });
}

function addFilter() {
    const line = document.createElement("div");
    const fieldSelector = document.createElement("select");
    line.appendChild(fieldSelector);
    setOptions(fieldSelector, fields);
    const valueSelector = document.createElement("select");
    line.appendChild(valueSelector);
    fieldSelector.onchange = () => {
        const field = fieldSelector.value;
        getUniqueValues(field, (list) => {
            if (fieldSelector.value === field) {
console.log("setting " + list.length + " values for " + field);
                setOptions(valueSelector, list);
            }
        });
    };
    fieldSelector.onchange();
    valueSelector.onchange = matchReferences;
    document.getElementById('filters').appendChild(line);
}

function init() {
    doJson("/hudoc/fields", null, function (err, json) {
        if (err) {
            return alert(err);
        }
        json.field.forEach((val) => fields.push(val));
        addFilter();
    });
}
init();
