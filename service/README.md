# HUDOC REST API

The code in this directory is a REST API to query information from the HUDOC database. The database contains records for court cases.

Three function are provided to access the information. The functions can be queried as XML or JSON service.

## Retrieving the list of record fields

A list of record fields can be obtained via a GET request to `/hudoc/fields`. The return format can be specified in the `Accept` header.

### Retrieve the list of record fields as XML

```bash
$ curl -H "Accept: text/xml" http://localhost:8090/hudoc/fields | xmllint --format -
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<get-fields-output xmlns="http://boecli.eu/hudoc/0.1">
  <field count="120">applicability</field>
  <field count="38374">appno</field>
  ...
  <field count="18">typedescription</field>
</get-fields-output>
```

### Retrieve the list of record fields as JSON

```bash
$ curl -H "Accept: application/json" http://localhost:8090/hudoc/fields | jq -M .
{
  "field": [
    {
      "count": 120,
      "value": "applicability"
    },
    {
      "count": 38374,
      "value": "appno"
    },
    ...
    {
      "count": 18,
      "value": "typedescription"
    }
  ]
}
```

## Retrieving all values for a particular field

All values of a field can be retrieved via a GET request.

### Retrieving all values for a particular field as XML

```bash
$ curl -H "Accept: text/xml" http://localhost:8090/hudoc/unique-values/doctype | xmllint --format -
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<get-unique-values-output xmlns="http://boecli.eu/hudoc/0.1">
  <value>HEDEC</value>
  <value>HEJUD</value>
  <value>HEREP</value>
</get-unique-values-output>
```

### Retrieving all values for a particular field as JSON

```bash
$ curl -H "Accept: application/json" http://localhost:8090/hudoc/unique-values/doctype | jq . -M
{
  "value": [
    "HEDEC",
    "HEJUD",
    "HEREP"
  ]
}
```

## Retrieve all records as XML

### Retrieve all records as XML

```bash
$ curl -s -H "Accept: text/xml" -H "Content-Type: text/xml" -d '<match-records-input xmlns="http://boecli.eu/hudoc/0.1"/>' http://localhost:8090/hudoc/match-records | xmllint --format -
```

### Retrieve all records as JSON

```bash
$ curl -H "Content-Type: application/json" -X POST -d '{}' http://localhost:8090/hudoc/match-records | jq . -M
```

## Retrieve matching records

### Retrieve matching records as XML

```bash
$ cat request.xml
<match-records-input xmlns="http://boecli.eu/hudoc/0.1">
  <field name="importance" value="2"/>
  <field name="respondent" value="ITA"/>
</match-records-input>
$ curl -v -H "Accept: text/xml" -H "Content-Type: text/xml" --data @request.xml http://localhost:8090/hudoc/match-records
```

### Retrieve matching records as JSON

```bash
$ cat request.json
{
  "field": [{
    "name": "importance",
    "value": 2
  }, {
    "name": "respondent",
    "value": "ITA"
  }]
}
$ curl -H "Content-Type: application/json" -X POST --data @request.json http://localhost:8090/hudoc/match-records | jq . -M
```

## Compiling and running the server

```bash
# compile to HUDOCAPI-0.1-SNAPSHOT.jar
mvn install
# run
java -cp target/HUDOCAPI-0.1-SNAPSHOT.jar eu.boecli.hudocapi.App
```

This runs a Jetty server on port 8090. See App.java.

There is a test interface available via [http://localhost:8090/test/](http://localhost:8090/test/).
It fires REST requests to the service.
It currently lets you filter from 1 value for 1 field at a time. For example, get all records with importance '2'.

## Implementation notes

The server loads the retrieved HUDOC records into a memory database. The recorded XML documents should be store in `../results` relative to the running service.
